/*
* grunt-roc-dependencies
*
*
* Copyright (c) 2014 bydooweedoo
* Licensed under the MIT license.
*/

'use strict';

module.exports = function(grunt) {

    var path = require('path'),
        apiPath = path.resolve(process.cwd(), 'cli/api'),
        api = require(apiPath + '/package'),
        description = 'ROC dependencies management.',
        cache = [];

    grunt.registerMultiTask('roc_dependencies', description, function() {

        var done = this.async(),
            roc = grunt.config('roc'),
            options = this.options({
                paths: [
                    roc.paths.framework,
                    roc.paths.config_file
                ]
            }),
            pkgName = this.target,
            pkgVersion = this.data,
            paths = options.paths,
            storage = roc.store || api.storage();

        if (paths.indexOf(roc.paths.config_file) === -1) {
            paths.push(roc.paths.config_file);
        }

        api.logger(grunt.log);
        api.storage(storage);

        function include_pkgs(success, pkgs) {

            var roc = grunt.config('roc');

            if (!cache.length) {
                cache = pkgs;
            }

            api.include(pkgs, pkgName, pkgVersion, options);
            roc.packages = api.storage();
            grunt.config('roc', roc);
            done();

        }

        if (cache.length === 0) {
            api.list(paths, include_pkgs, options);
        } else {
            include_pkgs(true, cache);
        }

        return true;

    });

};
